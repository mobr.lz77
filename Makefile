# lz77

include config.mk

SRC = lz77.c
OBJ = ${SRC:.c=.o}
TARGET = lz77

all: options ${TARGET}

options:
	@echo dices build options
	@echo "CFLAGS  = ${CFLAGS}"
	@echo "LDFLAGS = ${LDFLAGS}"
	@echo "CC      = ${CC}"
	@echo "LD      = ${LD}"

%.o: %.c
	@echo CC $<
	@${CC} ${CFLAGS} -c $<

${TARGET}: ${OBJ}
	@echo LD $@
	@${LD} ${LDFLAGS} -o $@ ${OBJ}

${OBJ}: config.mk

clean:
	@echo cleaning
	@rm -f ${TARGET} ${OBJ} ${TARGET}-${VERSION}.tar.gz

dist: clean
	@echo creating dist tarball
	@mkdir ${TARGET}-${VERSION}
	@cp LICENSE Makefile config.mk lz77.1 ${SRC} ${TARGET}-${VERSION}
	@tar cf ${TARGET}-${VERSION}.tar ${TARGET}-${VERSION}
	@gzip ${TARGET}-${VERSION}.tar
	@rm -r ${TARGET}-${VERSION}

install: all
	@echo "installing executable file to ${PREFIX}/bin"
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f ${TARGET} ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/${TARGET}
	@echo "installing manual page to ${MANPREFIX}/man1"
	@mkdir -p ${DESTDIR}${MANPREFIX}/man1
	@cp lz77.1 ${DESTDIR}${MANPREFIX}/man1/
	@chmod 644 ${DESTDIR}${MANPREFIX}/man1/lz77.1

uninstall:
	@echo "removing executable file from ${DESTDIR}${PREFIX}/bin"
	@rm -f ${DESTDIR}${PREFIX}/bin/${TARGET}
	@echo "removing manual page from ${DESTDIR}${MANPREFIX}/man1"
	@rm -f ${DESTDIR}${MANPREFIX}/man1/lz77.1

.PHONY: all options clean dist install uninstall
