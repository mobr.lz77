# lz77
VERSION = 2

# Customize below to fit your system

# paths
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

INCS = -I. -I/usr/include
LIBS =

# flags
CFLAGS  = -std=c99 -pedantic -Wall -Wextra -Os ${INCS}
CFLAGS += -DVERSION=\"${VERSION}\"
LDFLAGS = ${LIBS}

# compiler and linker
CC = cc
LD = ${CC}
