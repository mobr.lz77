#include <stdlib.h>
#include <stdio.h>
#include <string.h>

const char *progname;

struct pair {
  unsigned int distance;
  unsigned int length;
};

unsigned char *window;
size_t buffer_size, window_cap, buffer_cap;

void fill_buffer (void) {
  unsigned int i;
  int c;
  for (i = window_cap + buffer_size; i < window_cap + buffer_cap; i++) {
    c = getchar();
    if (c == EOF) {
      break;
    }
    window[i] = (unsigned char) c;
    buffer_size ++;
  }
}

void shift_buffer (unsigned int shift) {
  memmove (&window[0], &window[shift], window_cap + buffer_cap - shift);
  buffer_size -= shift;
}

unsigned int comlen(const unsigned char *str1
  , const unsigned char *str2
  , unsigned int limit) {
  unsigned int len = 0;
  while (*str1++ == *str2++ && len < limit)
    len ++;
  return len;
}

struct pair find_match (void) {
  unsigned int i;
  unsigned int clen;
  unsigned int dist, len;
  struct pair match;
  dist = 0;
  len  = 0;
  for (i = 0; i < window_cap; i++) {
    clen = comlen(&window[i], &window[window_cap], buffer_size - 1);
    if (clen >= len) {
      len = clen;
      dist = window_cap - i;
    }
  }
  match.distance = len ? dist : 0;
  match.length   = len;
  return match;
}

void write_pair (struct pair pair) {
  int c;
  c = pair.distance % 256;
  putchar(c);
  c = (pair.distance / 256) | (pair.length << 3);
  putchar(c);
}

void compress (void) {
  struct pair match;
  fill_buffer ();
  while (buffer_size) {
    match = find_match ();
    shift_buffer (match.length + 1);
    write_pair (match);
    putchar ((int) window[window_cap - 1]);
    fill_buffer ();
  }
}

int read_pair (struct pair *pair) {
  int c;
  int tmp;
  c = getchar ();
  if (c == EOF) {
    return 0;
  }
  tmp = c;
  c = getchar ();
  if (c == EOF) {
    fprintf(stderr, "%s: Unexpected end of file.\n", progname);
    exit (1);
  }
  tmp = tmp | ((c & 0x07) << 8);
  pair -> distance = tmp;
  pair -> length   = c >> 3;
  return 1;
}

void decompress (void) {
  struct pair pair;
  unsigned int i;
  int c;
  while (read_pair (&pair)) {
    c = getchar();
    if (c == EOF) {
      fprintf(stderr, "%s: Unexpected end of file.\n", progname);
      exit (1);
    }
    for (i = 0; i < pair.length; i++) {
      window[window_cap + i] = window[window_cap - pair.distance + i];
      buffer_size ++;
    }
    window[window_cap + buffer_size++] = (unsigned char) c;
    for (i = 0; i < buffer_size; i++) {
      putchar ((int) window[window_cap+i]);
    }
    shift_buffer (buffer_size);
  }
}

int main (int argc, char **argv) {
  progname = argv[0];
  window_cap = 2048;
  buffer_cap = 32;
  window = (unsigned char *) malloc (sizeof (unsigned char)
    * (window_cap + buffer_cap));
  if (window == NULL) {
    fprintf(stderr, "%s: Can not allocate memory.\n", progname);
    return 1;
  }
  memset (window, 0, window_cap + buffer_cap);
  if (argc == 2 && strcmp("-d", argv[1]) == 0) {
    decompress ();
  } else if (argc == 1) {
    compress ();
  } else {
    fprintf(stderr, "Usage: %s [-d]\n", progname);
    return 1;
  }
  free (window);
  return 0;
}
